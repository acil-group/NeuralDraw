#Author: Niklas Melton
#Date: 23/02/2016
#Basic MLP neural network visualization code

import numpy as np, math, pygame
from NeuralNets import MLP

white = 255,255,255
black = 0, 0, 0
red = 255,0,0
blue = 0,0,255
green = 0,255,0

class display:
    def __init__(self, x_b, y_b, borderSize = None):
        self.screen = pygame.display.set_mode((x_b,y_b))
        x_a = 0
        y_a = 0
        if borderSize is not None:
            x_a += borderSize
            x_b -= borderSize
            y_a += borderSize
            y_b -= borderSize
            self.x_a = x_a
            self.x_b = x_b
            self.y_a = y_a
            self.y_b = y_b
            self.drawList = []

    def pushDrawList(self, thing):
        thing.screen = self.screen
        self.drawList.append(thing)

    def draw(self, scale = 1):
        pygame.draw.rect(self.screen, white,[self.x_a,self.y_a,(self.x_b-self.x_a),(self.y_b-self.y_a)])
        y_inc  = (self.y_b-self.y_a)/len(self.drawList)
        i = 0
        for thing in self.drawList:
            i+=1
            thing.draw(self.x_a,self.x_b,self.y_a+(i-1)*y_inc,self.y_a+i*y_inc, scale)

class subDisplay:
    def __init__(self, screen, x_a, x_b, y_a, y_b, borderSize = None):
        oldSize = screen.get_size()
        if x_b > oldSize[0] or y_b > oldSize[1]:
            screen = pygame.display.set_mode((max(x_b,oldSize[0]),max(y_b,oldSize[1])))
        self.screen = screen
        if borderSize is not None:
            x_a += borderSize
            x_b -= borderSize
            y_a += borderSize
            y_b -= borderSize
        self.x_a = x_a
        self.x_b = x_b
        self.y_a = y_a
        self.y_b = y_b
        
        self.drawList = []

    def pushDrawList(self, thing):
        thing.screen = self.screen
        self.drawList.append(thing)

    def draw(self, scale = 1):
        pygame.draw.rect(self.screen, white,[self.x_a,self.y_a,(self.x_b-self.x_a),(self.y_b-self.y_a)])
        y_inc  = (self.y_b-self.y_a)/len(self.drawList)
        i = 0
        for thing in self.drawList:
            i+=1
            thing.draw(self.x_a,self.x_b,self.y_a+(i-1)*y_inc,self.y_a+i*y_inc, scale)



class network:
    def __init__(self, network):
        self.net = network
        self.screen = None

    def drawNodes(self, layer, x_a, x_b, y, c):
        x_inc = (x_b-x_a)/(self.net.truShape[layer]+1)
        i = 0
        for node in self.net.nodes[layer]:
            i+=1
            if node < 0:
                color = red
            else:
                color = blue
            pygame.draw.circle(self.screen,white,[int(x_a+i*x_inc),int(y)], c)
            pygame.draw.circle(self.screen,color,[int(x_a+i*x_inc),int(y)], min(int(c*abs(node)), 20))



    def drawWeights(self, layer,x_a,x_b,y_a,y_b, c):
        x_1_inc = (x_b-x_a)/(self.net.truShape[layer]+1)
        x_2_inc = (x_b-x_a)/(self.net.truShape[layer+1]+1)
        x2 = x_a
        for weightNode in self.net.weights[layer]:
            x2+=x_2_inc
            x1 = x_a
            for weight in weightNode:
                x1+=x_1_inc
                if weight < 0:
                    color = red
                else:
                    color = blue
                pygame.draw.line(self.screen, color,[int(x1),int(y_a)],[int(x2),int(y_b)],int(min(c*np.sqrt(abs(weight)),20)))
                
         
    def draw(self, x_a, x_b, y_a, y_b, c):
        y_inc = (y_b-y_a)/(len(self.net.truShape))
        ya = y_a+0.5*y_inc
        yb = ya+y_inc
        for layer in range(0,len(self.net.truShape)):
            if(layer != len(self.net.truShape)-1):
                self.drawWeights(layer,x_a,x_b,ya,yb,c)
            self.drawNodes(layer, x_a,x_b,ya,c)
            ya+=y_inc
            yb+=y_inc


            
